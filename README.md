#Lounge Stocking   

The office manager of a healthcare billing company has been charged with eliminating food waste in the office lounge. The lounge has packets of coffee creamer, each with an expiry date. The creamer must be used no later than that day. The manager also has the option to order additional coffee creamer from a grocery store, each with varying expiry dates. Given a fixed daily demand for creamer, find the maximum amount of additional creamer the manager can order without waste.   

Example:
onHand = [0, 2, 2]  
supplier = [2, 0, 0]  
demand = 2    
 
• The lounge has 3 units on hand expiring in [0,2,2] days, respectively  
• The store has an additional 3 units available, expiring in [2,0,0] days  
• Employees use at most 2 units of coffee creamer per day   

The function is expected to return an INTEGER 2.
