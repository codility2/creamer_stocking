(use 'clojure.test)
(load-file "main.clj")

(deftest all-used-in-one-day
  (let [on-hand [0 0 0]
        from-supplier [9 9 9]
        expected [(available-creamers on-hand from-supplier)]] 
    (is (= expected (ration-creamers 6 on-hand from-supplier)))))

(deftest onhand-used-first
  (let [same-available [0 0 0]
        expected [(with-priority priority-onhand same-available)]]
    (is (= expected 
           (ration-creamers (count same-available) same-available same-available)))))

(deftest expiring-from-supplier-skipped
  (let [on-hand [0 0 0] 
        from-supplier [9 0 0 9 9]
        n-daily 3
        expected (partition n-daily (available-creamers on-hand [9 9 9]))]
    (is (= expected (ration-creamers n-daily on-hand from-supplier)))))

(deftest expiring-on-hand-not-skipped
  (let [on-hand [0 0 0 1 1 1 1] 
        from-supplier [9 9 9]
        n-daily 3
        expected []]
    (is (= expected (ration-creamers n-daily on-hand from-supplier)))))

(deftest with-expiring-no-ration
  (let [on-hand [0 0 0 0] 
        from-supplier [9 0 0 9 9]
        n-daily 3
        expected []]
    (is (= expected (ration-creamers n-daily on-hand from-supplier)))))

(deftest zero-consumption
    (is (= [] (ration-creamers 0 [1 1] [2 2]))))

(deftest average-case
  (let [on-hand [1 0 1]
        from-supplier [2 0 2 0 0 2]
        n-daily 2
        expected (partition n-daily (available-creamers [0 1 1] [0 2 2]))]
    (is (= expected (ration-creamers n-daily on-hand from-supplier)))))

(run-tests)
