(defonce priority-onhand :A)
(defonce priority-supplier :B)

(defmacro expires[creamer]
  `(first ~creamer))

(defmacro priority[creamer]
  `(second ~creamer))

(defmacro with-priority[priority creamers]
  `(map #(vector % ~priority) ~creamers))

(defmacro has-expiring-on[day creamers]
  `(let [creamer# (first ~creamers)]
     (and (= ~priority-onhand (priority creamer#))
          (= ~day (expires creamer#)))))

(defmacro drop-unconsumed[day creamers]
  `(drop-while #(= ~day (expires %)) ~creamers))

(defmacro available-creamers[on-hand in-stock]
  `(sort (concat (with-priority priority-onhand ~on-hand) 
                 (with-priority priority-supplier ~in-stock))))

(defn from-supplier?[creamer]
  (= priority-supplier (priority creamer)))

(defn ration-creamers[n-daily on-hand from-supplier]
  (if (< n-daily 1)
    []
    (loop [day 0 remaining (available-creamers on-hand from-supplier) daily-portions []]
      (if (not-empty remaining)
        (let [[portion, remains] (split-at n-daily remaining)]
          (if (has-expiring-on day remains) 
            []
            (recur (inc day) (drop-unconsumed day remains) (conj daily-portions portion))))
       daily-portions))))

(defn missing-creamer-count[on-hand from-supplier n-daily]
  (if-let [ration (seq (ration-creamers n-daily on-hand from-supplier))]
    (count (filter from-supplier? (apply concat ration)))
    -1))

